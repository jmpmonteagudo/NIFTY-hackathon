(function(f){if(typeof exports==="object"&&typeof module!=="undefined"){module.exports=f()}else if(typeof define==="function"&&define.amd){define([],f)}else{var g;if(typeof window!=="undefined"){g=window}else if(typeof global!=="undefined"){g=global}else if(typeof self!=="undefined"){g=self}else{g=this}g.UserInput = f()}})(function(){var define,module,exports;return (function(){function r(e,n,t){function o(i,f){if(!n[i]){if(!e[i]){var c="function"==typeof require&&require;if(!f&&c)return c(i,!0);if(u)return u(i,!0);var a=new Error("Cannot find module '"+i+"'");throw a.code="MODULE_NOT_FOUND",a}var p=n[i]={exports:{}};e[i][0].call(p.exports,function(r){var n=e[i][1][r];return o(n||r)},p,p.exports,r,e,n,t)}return n[i].exports}for(var u="function"==typeof require&&require,i=0;i<t.length;i++)o(t[i]);return o}return r})()({1:[function(require,module,exports){
const messageSequenceAbi = require('./message-sequence-abi.js')

function startApp() {
    initAddress()
    initContract()
    displayBlockLoop()
}

window.addEventListener('load', function () {
    if (typeof web3 !== 'undefined') {
        startApp();
    } else {
        document.write("Sorry, example this won't work unless you use a web3 browser or install metamask")
    }
})

function initAddress() {
    web3.eth.getAccounts((err, accounts) => {
        document.querySelector('#from').value = accounts[0]
    })
}

function displayBlockLoop() {
    web3.eth.getBlockNumber((err, blockNumber) => {
        if (blockNumber) {
            document.querySelector('#currentBlock').innerText = blockNumber.toString()
            setTimeout(displayBlockLoop, 2000)
        }
    })
}

function initContract() {
    const address = document.querySelector('#contract').value
    const MessageSequence = web3.eth.contract(messageSequenceAbi);
    const messageSequence = MessageSequence.at(address)

    let event = messageSequence.MessageChange()
    event.watch((error, result) => {
        if (result) {
            console.log("Event result: " + result.event)
            let args = result.args
            document.querySelector('#eventMessage').innerText = `Message changed to "${args['newMessage']}" from "${args['oldMessage']}" by account ${args['sender']}`
        } else if (error) {
            console.error("Event error: " + error)
        }
    })

    listenForClicks(messageSequence)
}

function handleTransactionRequest(txHash) {
    console.info(`Checking for transaction completion of ${txHash}`)
    web3.eth.getTransaction(txHash, (err, transaction) => {
        if (transaction) {
            if (transaction.transactionIndex) {
                console.info(`Transaction complete ${txHash}`)
                document.querySelector('#statusMessage').innerText = `Mined on block ${transaction.blockNumber} with transaction index ${transaction.transactionIndex}`
            } else {
                console.info(`Transaction not yet complete ${txHash}`)
                window.setTimeout(() => handleTransactionRequest(txHash), 2000)
            }
        } else if (err) {
            console.log(`getTransaction callback error: ${err} /`)
        }
    })
}

function listenForClicks(messageSequence) {
    let button = document.querySelector('#send')

    button.addEventListener('click', function () {
        let message = document.querySelector('#message').value
        let fromAddress = document.querySelector('#from').value

        messageSequence.updateMessage(message, {from: fromAddress}, (err, txHash) => {
            document.querySelector('#statusMessage').innerText = `Message sent to blockchain with transaction ${txHash}`
            handleTransactionRequest(txHash)
        })
    })
}
},{"./message-sequence-abi.js":2}],2:[function(require,module,exports){
module.exports = [
    {
        "constant": false,
        "inputs": [
            {
                "name": "newMessage",
                "type": "string"
            }
        ],
        "name": "updateMessage",
        "outputs": [
            {
                "name": "",
                "type": "string"
            }
        ],
        "payable": false,
        "stateMutability": "nonpayable",
        "type": "function"
    },
    {
        "constant": true,
        "inputs": [

        ],
        "name": "message",
        "outputs": [
            {
                "name": "",
                "type": "string"
            }
        ],
        "payable": false,
        "stateMutability": "view",
        "type": "function"
    },
    {
        "anonymous": false,
        "inputs": [
            {
                "indexed": true,
                "name": "sender",
                "type": "address"
            },
            {
                "indexed": false,
                "name": "oldMessage",
                "type": "string"
            },
            {
                "indexed": false,
                "name": "newMessage",
                "type": "string"
            }
        ],
        "name": "MessageChange",
        "type": "event"
    }
]

},{}]},{},[1])(1)
});

//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIm5vZGVfbW9kdWxlcy9icm93c2VyLXBhY2svX3ByZWx1ZGUuanMiLCJzcmMvaW5kZXguanMiLCJzcmMvbWVzc2FnZS1zZXF1ZW5jZS1hYmkuanMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7QUNBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQy9FQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EiLCJmaWxlIjoiZ2VuZXJhdGVkLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXNDb250ZW50IjpbIihmdW5jdGlvbigpe2Z1bmN0aW9uIHIoZSxuLHQpe2Z1bmN0aW9uIG8oaSxmKXtpZighbltpXSl7aWYoIWVbaV0pe3ZhciBjPVwiZnVuY3Rpb25cIj09dHlwZW9mIHJlcXVpcmUmJnJlcXVpcmU7aWYoIWYmJmMpcmV0dXJuIGMoaSwhMCk7aWYodSlyZXR1cm4gdShpLCEwKTt2YXIgYT1uZXcgRXJyb3IoXCJDYW5ub3QgZmluZCBtb2R1bGUgJ1wiK2krXCInXCIpO3Rocm93IGEuY29kZT1cIk1PRFVMRV9OT1RfRk9VTkRcIixhfXZhciBwPW5baV09e2V4cG9ydHM6e319O2VbaV1bMF0uY2FsbChwLmV4cG9ydHMsZnVuY3Rpb24ocil7dmFyIG49ZVtpXVsxXVtyXTtyZXR1cm4gbyhufHxyKX0scCxwLmV4cG9ydHMscixlLG4sdCl9cmV0dXJuIG5baV0uZXhwb3J0c31mb3IodmFyIHU9XCJmdW5jdGlvblwiPT10eXBlb2YgcmVxdWlyZSYmcmVxdWlyZSxpPTA7aTx0Lmxlbmd0aDtpKyspbyh0W2ldKTtyZXR1cm4gb31yZXR1cm4gcn0pKCkiLCJjb25zdCBtZXNzYWdlU2VxdWVuY2VBYmkgPSByZXF1aXJlKCcuL21lc3NhZ2Utc2VxdWVuY2UtYWJpLmpzJylcblxuZnVuY3Rpb24gc3RhcnRBcHAoKSB7XG4gICAgaW5pdEFkZHJlc3MoKVxuICAgIGluaXRDb250cmFjdCgpXG4gICAgZGlzcGxheUJsb2NrTG9vcCgpXG59XG5cbndpbmRvdy5hZGRFdmVudExpc3RlbmVyKCdsb2FkJywgZnVuY3Rpb24gKCkge1xuICAgIGlmICh0eXBlb2Ygd2ViMyAhPT0gJ3VuZGVmaW5lZCcpIHtcbiAgICAgICAgc3RhcnRBcHAoKTtcbiAgICB9IGVsc2Uge1xuICAgICAgICBkb2N1bWVudC53cml0ZShcIlNvcnJ5LCBleGFtcGxlIHRoaXMgd29uJ3Qgd29yayB1bmxlc3MgeW91IHVzZSBhIHdlYjMgYnJvd3NlciBvciBpbnN0YWxsIG1ldGFtYXNrXCIpXG4gICAgfVxufSlcblxuZnVuY3Rpb24gaW5pdEFkZHJlc3MoKSB7XG4gICAgd2ViMy5ldGguZ2V0QWNjb3VudHMoKGVyciwgYWNjb3VudHMpID0+IHtcbiAgICAgICAgZG9jdW1lbnQucXVlcnlTZWxlY3RvcignI2Zyb20nKS52YWx1ZSA9IGFjY291bnRzWzBdXG4gICAgfSlcbn1cblxuZnVuY3Rpb24gZGlzcGxheUJsb2NrTG9vcCgpIHtcbiAgICB3ZWIzLmV0aC5nZXRCbG9ja051bWJlcigoZXJyLCBibG9ja051bWJlcikgPT4ge1xuICAgICAgICBpZiAoYmxvY2tOdW1iZXIpIHtcbiAgICAgICAgICAgIGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3IoJyNjdXJyZW50QmxvY2snKS5pbm5lclRleHQgPSBibG9ja051bWJlci50b1N0cmluZygpXG4gICAgICAgICAgICBzZXRUaW1lb3V0KGRpc3BsYXlCbG9ja0xvb3AsIDIwMDApXG4gICAgICAgIH1cbiAgICB9KVxufVxuXG5mdW5jdGlvbiBpbml0Q29udHJhY3QoKSB7XG4gICAgY29uc3QgYWRkcmVzcyA9IGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3IoJyNjb250cmFjdCcpLnZhbHVlXG4gICAgY29uc3QgTWVzc2FnZVNlcXVlbmNlID0gd2ViMy5ldGguY29udHJhY3QobWVzc2FnZVNlcXVlbmNlQWJpKTtcbiAgICBjb25zdCBtZXNzYWdlU2VxdWVuY2UgPSBNZXNzYWdlU2VxdWVuY2UuYXQoYWRkcmVzcylcblxuICAgIGxldCBldmVudCA9IG1lc3NhZ2VTZXF1ZW5jZS5NZXNzYWdlQ2hhbmdlKClcbiAgICBldmVudC53YXRjaCgoZXJyb3IsIHJlc3VsdCkgPT4ge1xuICAgICAgICBpZiAocmVzdWx0KSB7XG4gICAgICAgICAgICBjb25zb2xlLmxvZyhcIkV2ZW50IHJlc3VsdDogXCIgKyByZXN1bHQuZXZlbnQpXG4gICAgICAgICAgICBsZXQgYXJncyA9IHJlc3VsdC5hcmdzXG4gICAgICAgICAgICBkb2N1bWVudC5xdWVyeVNlbGVjdG9yKCcjZXZlbnRNZXNzYWdlJykuaW5uZXJUZXh0ID0gYE1lc3NhZ2UgY2hhbmdlZCB0byBcIiR7YXJnc1snbmV3TWVzc2FnZSddfVwiIGZyb20gXCIke2FyZ3NbJ29sZE1lc3NhZ2UnXX1cIiBieSBhY2NvdW50ICR7YXJnc1snc2VuZGVyJ119YFxuICAgICAgICB9IGVsc2UgaWYgKGVycm9yKSB7XG4gICAgICAgICAgICBjb25zb2xlLmVycm9yKFwiRXZlbnQgZXJyb3I6IFwiICsgZXJyb3IpXG4gICAgICAgIH1cbiAgICB9KVxuXG4gICAgbGlzdGVuRm9yQ2xpY2tzKG1lc3NhZ2VTZXF1ZW5jZSlcbn1cblxuZnVuY3Rpb24gaGFuZGxlVHJhbnNhY3Rpb25SZXF1ZXN0KHR4SGFzaCkge1xuICAgIGNvbnNvbGUuaW5mbyhgQ2hlY2tpbmcgZm9yIHRyYW5zYWN0aW9uIGNvbXBsZXRpb24gb2YgJHt0eEhhc2h9YClcbiAgICB3ZWIzLmV0aC5nZXRUcmFuc2FjdGlvbih0eEhhc2gsIChlcnIsIHRyYW5zYWN0aW9uKSA9PiB7XG4gICAgICAgIGlmICh0cmFuc2FjdGlvbikge1xuICAgICAgICAgICAgaWYgKHRyYW5zYWN0aW9uLnRyYW5zYWN0aW9uSW5kZXgpIHtcbiAgICAgICAgICAgICAgICBjb25zb2xlLmluZm8oYFRyYW5zYWN0aW9uIGNvbXBsZXRlICR7dHhIYXNofWApXG4gICAgICAgICAgICAgICAgZG9jdW1lbnQucXVlcnlTZWxlY3RvcignI3N0YXR1c01lc3NhZ2UnKS5pbm5lclRleHQgPSBgTWluZWQgb24gYmxvY2sgJHt0cmFuc2FjdGlvbi5ibG9ja051bWJlcn0gd2l0aCB0cmFuc2FjdGlvbiBpbmRleCAke3RyYW5zYWN0aW9uLnRyYW5zYWN0aW9uSW5kZXh9YFxuICAgICAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgICAgICBjb25zb2xlLmluZm8oYFRyYW5zYWN0aW9uIG5vdCB5ZXQgY29tcGxldGUgJHt0eEhhc2h9YClcbiAgICAgICAgICAgICAgICB3aW5kb3cuc2V0VGltZW91dCgoKSA9PiBoYW5kbGVUcmFuc2FjdGlvblJlcXVlc3QodHhIYXNoKSwgMjAwMClcbiAgICAgICAgICAgIH1cbiAgICAgICAgfSBlbHNlIGlmIChlcnIpIHtcbiAgICAgICAgICAgIGNvbnNvbGUubG9nKGBnZXRUcmFuc2FjdGlvbiBjYWxsYmFjayBlcnJvcjogJHtlcnJ9IC9gKVxuICAgICAgICB9XG4gICAgfSlcbn1cblxuZnVuY3Rpb24gbGlzdGVuRm9yQ2xpY2tzKG1lc3NhZ2VTZXF1ZW5jZSkge1xuICAgIGxldCBidXR0b24gPSBkb2N1bWVudC5xdWVyeVNlbGVjdG9yKCcjc2VuZCcpXG5cbiAgICBidXR0b24uYWRkRXZlbnRMaXN0ZW5lcignY2xpY2snLCBmdW5jdGlvbiAoKSB7XG4gICAgICAgIGxldCBtZXNzYWdlID0gZG9jdW1lbnQucXVlcnlTZWxlY3RvcignI21lc3NhZ2UnKS52YWx1ZVxuICAgICAgICBsZXQgZnJvbUFkZHJlc3MgPSBkb2N1bWVudC5xdWVyeVNlbGVjdG9yKCcjZnJvbScpLnZhbHVlXG5cbiAgICAgICAgbWVzc2FnZVNlcXVlbmNlLnVwZGF0ZU1lc3NhZ2UobWVzc2FnZSwge2Zyb206IGZyb21BZGRyZXNzfSwgKGVyciwgdHhIYXNoKSA9PiB7XG4gICAgICAgICAgICBkb2N1bWVudC5xdWVyeVNlbGVjdG9yKCcjc3RhdHVzTWVzc2FnZScpLmlubmVyVGV4dCA9IGBNZXNzYWdlIHNlbnQgdG8gYmxvY2tjaGFpbiB3aXRoIHRyYW5zYWN0aW9uICR7dHhIYXNofWBcbiAgICAgICAgICAgIGhhbmRsZVRyYW5zYWN0aW9uUmVxdWVzdCh0eEhhc2gpXG4gICAgICAgIH0pXG4gICAgfSlcbn0iLCJtb2R1bGUuZXhwb3J0cyA9IFtcbiAgICB7XG4gICAgICAgIFwiY29uc3RhbnRcIjogZmFsc2UsXG4gICAgICAgIFwiaW5wdXRzXCI6IFtcbiAgICAgICAgICAgIHtcbiAgICAgICAgICAgICAgICBcIm5hbWVcIjogXCJuZXdNZXNzYWdlXCIsXG4gICAgICAgICAgICAgICAgXCJ0eXBlXCI6IFwic3RyaW5nXCJcbiAgICAgICAgICAgIH1cbiAgICAgICAgXSxcbiAgICAgICAgXCJuYW1lXCI6IFwidXBkYXRlTWVzc2FnZVwiLFxuICAgICAgICBcIm91dHB1dHNcIjogW1xuICAgICAgICAgICAge1xuICAgICAgICAgICAgICAgIFwibmFtZVwiOiBcIlwiLFxuICAgICAgICAgICAgICAgIFwidHlwZVwiOiBcInN0cmluZ1wiXG4gICAgICAgICAgICB9XG4gICAgICAgIF0sXG4gICAgICAgIFwicGF5YWJsZVwiOiBmYWxzZSxcbiAgICAgICAgXCJzdGF0ZU11dGFiaWxpdHlcIjogXCJub25wYXlhYmxlXCIsXG4gICAgICAgIFwidHlwZVwiOiBcImZ1bmN0aW9uXCJcbiAgICB9LFxuICAgIHtcbiAgICAgICAgXCJjb25zdGFudFwiOiB0cnVlLFxuICAgICAgICBcImlucHV0c1wiOiBbXG5cbiAgICAgICAgXSxcbiAgICAgICAgXCJuYW1lXCI6IFwibWVzc2FnZVwiLFxuICAgICAgICBcIm91dHB1dHNcIjogW1xuICAgICAgICAgICAge1xuICAgICAgICAgICAgICAgIFwibmFtZVwiOiBcIlwiLFxuICAgICAgICAgICAgICAgIFwidHlwZVwiOiBcInN0cmluZ1wiXG4gICAgICAgICAgICB9XG4gICAgICAgIF0sXG4gICAgICAgIFwicGF5YWJsZVwiOiBmYWxzZSxcbiAgICAgICAgXCJzdGF0ZU11dGFiaWxpdHlcIjogXCJ2aWV3XCIsXG4gICAgICAgIFwidHlwZVwiOiBcImZ1bmN0aW9uXCJcbiAgICB9LFxuICAgIHtcbiAgICAgICAgXCJhbm9ueW1vdXNcIjogZmFsc2UsXG4gICAgICAgIFwiaW5wdXRzXCI6IFtcbiAgICAgICAgICAgIHtcbiAgICAgICAgICAgICAgICBcImluZGV4ZWRcIjogdHJ1ZSxcbiAgICAgICAgICAgICAgICBcIm5hbWVcIjogXCJzZW5kZXJcIixcbiAgICAgICAgICAgICAgICBcInR5cGVcIjogXCJhZGRyZXNzXCJcbiAgICAgICAgICAgIH0sXG4gICAgICAgICAgICB7XG4gICAgICAgICAgICAgICAgXCJpbmRleGVkXCI6IGZhbHNlLFxuICAgICAgICAgICAgICAgIFwibmFtZVwiOiBcIm9sZE1lc3NhZ2VcIixcbiAgICAgICAgICAgICAgICBcInR5cGVcIjogXCJzdHJpbmdcIlxuICAgICAgICAgICAgfSxcbiAgICAgICAgICAgIHtcbiAgICAgICAgICAgICAgICBcImluZGV4ZWRcIjogZmFsc2UsXG4gICAgICAgICAgICAgICAgXCJuYW1lXCI6IFwibmV3TWVzc2FnZVwiLFxuICAgICAgICAgICAgICAgIFwidHlwZVwiOiBcInN0cmluZ1wiXG4gICAgICAgICAgICB9XG4gICAgICAgIF0sXG4gICAgICAgIFwibmFtZVwiOiBcIk1lc3NhZ2VDaGFuZ2VcIixcbiAgICAgICAgXCJ0eXBlXCI6IFwiZXZlbnRcIlxuICAgIH1cbl1cbiJdfQ==
