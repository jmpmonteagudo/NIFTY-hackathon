# NIFTY hackathon - CryptoCarz 

## Inspiration
 
We took inspiration for our dapp from the following:
The VR game we are building, CryptoCarz
Decentraland SDK
Fast and Furious the movie
Prediction Markets
Cryptocurrency market price dynamics
Non-Fungible Tokens protocols and ideas
 
 
## What it does
 
We built a drag racing game between 4 cars inside Decentraland. Some of the game mechanics are defined and controlled through NFTs and smart contracts. An app provides the ability to bet on the race results.

 
## How we built it

We used Decentraland SDK to simulate the race.
We developed and deployed the Ethereum smart contracts using Truffle.
For the betting Dapp, we use Metamask and Toshi.


## Challenges we ran into

None of us had previous experience with Decentraland SDK, so it was difficult for us to pick it up quickly.
The documentation is not very exhaustive either. But overall the platform is great and we were able to run a simple straight-line race.


## What we are proud of
We are the first racing game on the decentralized virtual world!
Our team worked very hard to complete the Decentraland SDK and smart contract code on time.


## What we have learned
 
We have got some CryptoCarz racing!
We learned a lot about Decentralnd SDK and how to integrate Dapps into Toshi, and more to come!


## What's next to come for CryptoCarz Fast&Furious Dapp
 
Implementation of multisig walllets to split entry fee for the tournaments between different entities such track owners, tournament organizers, platform providers, etc
Integration with market data feed from multiple exchanges, weighted by volume to smoothen out data feed
Implement market data feed using DEX feed to further decentralize the game, when DEX volume will be sufficiently high.
Implement odds for the betting, possibly integrating with existing prediction market platforms like Augur or Gnosis
Adding more complex geometries for the track where other characteristics of the car impact the race, such cornering speed etc.


## Contracts

Please see the [contracts/](truffle/contracts) directory.

## Develop

Contracts are written in [Solidity][solidity] and tested using [Truffle][truffle] and [ganache-cli][ganache-cli].

### Dependencies

```bash
# Install local node dependencies
$ npm install
```

### Test

```bash
# Compile all smart contracts
$ npm run build

# Run all tests
$ npm test

# Run test coverage analysis
$ npm run coverage
```

### Docker

A Docker image to run containerized testing is provided. Requires [Docker Compose][docker compose].

```bash
# Build the container and run all tests
$ make build test

# Run a test for a single contract
$ docker-compose run --rm truffle npm test test/CryptoCarzToken.test.js
```

### Smart Contract Addresses on Kovan

```
tokenCars = 0x9cc230a044c50a7e13a8a47148942063307907cc
tokenTracks = 0x342a80ba02404326a1f8fc288cee179700128383
oracle = 0xa73b500bec474c348aea66882e073a077a247c26
race = 0x2ca6022b682427424f864f076927fe7f867fa93d
betting = 0x1ce46e9d15dc46fb41c08209fed4b22218b13e3a
```

[ethereum]: https://www.ethereum.org/
[solidity]: https://solidity.readthedocs.io/en/develop/
[truffle]: http://truffleframework.com/
[ganache-cli]: https://github.com/trufflesuite/ganache-cli