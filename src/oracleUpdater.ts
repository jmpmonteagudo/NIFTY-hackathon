
import * as Web3 from 'web3';
import * as EthereumTx from 'ethereumjs-tx';
import * as SolidityFunction from 'web3/lib/web3/function';
import * as lodash from 'lodash';

const PRIVATE_KEY = process.env.PRIVATE_KEY;
const ORACLE_CONTRACT_ADDRESS = process.env.ORACLE_CONTRACT_ADDRESS;
const RPC_SERVER_ADDRESS = process.env.RPC_SERVER_ADDRESS;
//'https://rinkeby.infura.io';
//const RPC_SERVER_ADDRESS = 'https://ropsten.infura.io';
//const RPC_SERVER_ADDRESS = 'http://localhost:8545';


function run() {
    console.log('asdadasda');
}

/*
function getWeb3() {
    const web3 = new Web3(new Web3.providers.HttpProvider(RPC_SERVER_ADDRESS));
    console.log(`web3.eth.blockNumber = ${web3.eth.blockNumber}`);
    return web3;
}

function getContractRawData(contractName: string): any {
    const path = `../truffle/build/contracts/${contractName}.json`;
    return require(path);
}

function createContract(contractName: string) {
    const rawData = getContractRawData(contractName);
    console.log(`rawData = ${rawData}`);
    console.log(`rawData['abi'] = ${rawData['abi']}`);
    return getWeb3().eth.contract([rawData['abi']]);
}

async function createSignedSmartContractCreationTransaction(
    fromPrivateKey: string, abi: any[], bytecode: string, constructorParameters: any[],
    nonce: number, gasPrice: number, gasEstimate: number) {

    let contractData;
    if (constructorParameters.length > 0) {
        const Contract = getWeb3().eth.contract(abi);
        constructorParameters.push({ data: bytecode });
        contractData = Contract.new.getData(...constructorParameters);
    } else {
        contractData = bytecode;
    }

    const privateKeyBuff = new Buffer(fromPrivateKey, 'hex');
    const rawTx = {
        nonce: nonce,
        gasPrice: gasPrice,
        gasLimit: gasEstimate,
        value: '0x00',
        data: contractData
    };
    const tx = new EthereumTx(rawTx);
    tx.sign(privateKeyBuff);
    return '0x' + tx.serialize().toString('hex');
}

async function deployContract(contractName: string): Promise<string> {
    console.log(`deployContract`);
    const rawData = await getContractRawData(contractName);
    const fromPrivateKey = PRIVATE_KEY;
    const nonce = getWeb3().eth.getTransactionCount(ADDRESS);
    const gasPriceGWei = 1;
    const gasPrice = gasPriceGWei * 10 ** 9;
    const abi = rawData['abi'];
    const bytecode = rawData['bytecode'];
    const constructorParameters = ['0xcE1b28c91391E29ce0c69172Fe992793c1B4Ad96', '0x7d068eB54D3160a24e3a80A4C4D148d07FB3F4e1'];
    const gasEstimate = 4000000;

    const serializedTx = await createSignedSmartContractCreationTransaction(
        fromPrivateKey, abi, bytecode, constructorParameters,
        nonce, gasPrice, gasEstimate);
    console.log(`serializedTx.length = ${serializedTx.length}`);
    const txHash = await getWeb3().eth.sendRawTransaction(serializedTx);
    console.log(`txHash = ${txHash}`);
    return txHash;
}

async function createSignedFunctionCallTransaction(
    contract: any, methodName: string, methodParams: any[],
    gasPrice: number, gasEstimate: number) {

    const abi = contract.abi;
    const solidityFunction = new SolidityFunction('', lodash.find(abi, { name: methodName }), '');
    const payloadData = solidityFunction.toPayload(methodParams).data;

    const nonce = getWeb3().eth.getTransactionCount(ADDRESS);

    const privateKeyBuff = new Buffer(PRIVATE_KEY, 'hex');
    const rawTx = {
        nonce,
        gasPrice,
        gasLimit: gasEstimate,
        to: contract.address,
        value: '0x00',
        data: payloadData
    };
    const tx = new EthereumTx(rawTx);
    tx.sign(privateKeyBuff);
    return '0x' + tx.serialize().toString('hex');
}

async function createAuction(tokenContract: any) {
    const serializedTx = await createSignedFunctionCallTransaction(
        tokenContract, 'createAuction', [], 10 ** 9, 1500000);
    console.log(`serializedTx = ${serializedTx}`);
    console.log(`serializedTx.length = ${serializedTx.length}`);
    const txHash = await getWeb3().eth.sendRawTransaction(serializedTx);
    console.log(`txHash = ${txHash}`);
}

async function loadContractAndCreateAuction() {
    const web3 = getWeb3();
    const abiArray = 
    ];
    let tokenDefinition = web3.eth.contract(abiArray);

    console.log(`tokenDefinition = ${tokenDefinition}`);
    const tokenContract = await tokenDefinition.at(TOKEN_CONTRACT_ADDRESS);
    console.log(`tokenContract = ${tokenContract}`);

    const owner = await tokenContract.owner.call();
    console.log(`owner = ${owner}`);

    await createAuction(tokenContract);
}

function test() {
    //deployContract('CryptoCarzToken');
    loadContractAndCreateAuction();
}

test();
*/