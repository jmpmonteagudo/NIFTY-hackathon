#!/bin/bash

set -e
set -x

set -a
. env/run.env
set +a

ts-node src/run.ts
