const fs = require('fs');

const data = JSON.parse(fs.readFileSync('build/contractAddresses.txt'));

function replaceBetweenFile(filePath, needle1, needle2, replacing, filePath2) {
    const fileContents = fs.readFileSync(filePath, 'utf8');
    const newFileContents = replaceBetween(fileContents, needle1, needle2, replacing);
    fs.writeFileSync(filePath2, newFileContents);
}

function replaceBetween(haystack, needle1, needle2, replacing) {
    const n1 = haystack.indexOf(needle1);
    const haystack2 = haystack.substring(n1 + needle1.length, haystack.length);
    const n2 = haystack2.indexOf(needle2);
    return haystack.substring(0, n1 + needle1.length) +
        replacing + haystack2.substring(n2, haystack2.length);
}

//const haystack = "hello hola hello";
//console.log(`${replaceBetween(haystack, 'llo ', ' he', 'adiosito')}`);


replaceBetweenFile(
    'examples/index.html',
    'var bettingContractAddr = bettingContract.at("',
    '");',
    data["CryptoCarzBetting"],
    'examples/index.html');



const scData = require('../truffle/build/contracts/CryptoCarzBetting.json');
const abiTxt = JSON.stringify(scData['abi']);
replaceBetweenFile(
    'examples/index.html',
    'var abi = ',
    ';',
    abiTxt,
    'examples/index.html');

// console.log(`data = ${JSON.stringify(data, null, 4)}`);

