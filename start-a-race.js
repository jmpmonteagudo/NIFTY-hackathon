owner = web3.eth.accounts[0];
manager = web3.eth.accounts[1];
treasurer = web3.eth.accounts[2];
someoneElse = web3.eth.accounts[3];
trackOwner = web3.eth.accounts[4];
participants = users = [ web3.eth.accounts[5], web3.eth.accounts[6], web3.eth.accounts[7], web3.eth.accounts[8]]
web3.eth.sendTransaction({from: web3.eth.accounts[0], to:"0xb11a21f4cec2c954df533a16d682de73c423205e", value: web3.toWei(1)})
web3.eth.sendTransaction({from: web3.eth.accounts[0], to:"0x7d3150447026e92758564d71c7e8f602e682c416", value: web3.toWei(1)})
CAR_IDS = [1, 2, 3, 4];

CryptoCarzToken.new(owner, manager, treasurer, { from: someoneElse }).then(x => global.tokenCars = x)

CryptoCarzTokenTrack.new(owner, manager, { from: someoneElse }).then(x => global.tokenTracks = x)

CryptoCarzOracle.new(owner, manager, { from: someoneElse }).then(x => global.oracle = x)

CryptoCarzRace.new( owner, manager, tokenCars.address, tokenTracks.address, oracle.address, { from: someoneElse }).then(x => global.race = x)

CryptoCarzBetting.new(owner, manager, race.address).then(x => global.betting = x)

web3.eth.sendTransaction({from: web3.eth.accounts[0], to: betting.address, value: web3.toWei(5)})

console.log(`tokenCars.address = ${tokenCars.address}`)
console.log(`tokenTracks.address = ${tokenTracks.address}`)
console.log(`oracle.address = ${oracle.address}`)
console.log(`race.address = ${race.address}`)
console.log(`betting.address = ${betting.address}`)

tokenCars.createSeries(CAR_IDS.length, { from: manager });
tokenCars.createCars(CAR_IDS, 0, { from: manager });
tokenTracks.createTrack(1, { from: manager });

BigNumber = require('bignumber.js')
ETHER = new BigNumber('1e18');
DEC18 = new BigNumber('1e18');
oracle.updateRate("BTC", DEC18.times(1), { from: manager });
oracle.updateRate("ETH", DEC18.times(1), { from: manager });
oracle.updateRate("XRP", DEC18.times(1), { from: manager });
oracle.updateRate("BCH", DEC18.times(1), { from: manager });

tokenCars.transferFrom(manager, participants[0], CAR_IDS[0], { from: manager })
tokenCars.transferFrom(manager, participants[1], CAR_IDS[1], { from: manager })
tokenCars.transferFrom(manager, participants[2], CAR_IDS[2], { from: manager })
tokenCars.transferFrom(manager, participants[3], CAR_IDS[3], { from: manager })
tokenTracks.transferFrom(manager, trackOwner, 1, { from: manager });


// *****
// ** SET UP IS DONE **
// *****


race.signUp(CAR_IDS[0], { from: participants[0], value: ETHER.times(1)});
race.signUp(CAR_IDS[1], { from: participants[1], value: ETHER.times(1)});
race.signUp(CAR_IDS[2], { from: participants[2], value: ETHER.times(1)});
race.signUp(CAR_IDS[3], { from: participants[3], value: ETHER.times(1)});

tokenCars.transferFrom(participants[0], race.address, CAR_IDS[0], { from: participants[0] });
tokenCars.transferFrom(participants[1], race.address, CAR_IDS[1], { from: participants[1] });
tokenCars.transferFrom(participants[2], race.address, CAR_IDS[2], { from: participants[2] });
tokenCars.transferFrom(participants[3], race.address, CAR_IDS[3], { from: participants[3] });


/// ***************************8
// Bet now
// Bet now
// Bet now
// Bet now
/// ***************************8

web3.currentProvider.send({ jsonrpc: '2.0', method: 'evm_mine', id: Date.now(), });
web3.currentProvider.send({ jsonrpc: '2.0', method: 'evm_mine', id: Date.now(), });
web3.currentProvider.send({ jsonrpc: '2.0', method: 'evm_mine', id: Date.now(), });
web3.currentProvider.send({ jsonrpc: '2.0', method: 'evm_mine', id: Date.now(), });
web3.currentProvider.send({ jsonrpc: '2.0', method: 'evm_mine', id: Date.now(), });
web3.currentProvider.send({ jsonrpc: '2.0', method: 'evm_mine', id: Date.now(), });
web3.currentProvider.send({ jsonrpc: '2.0', method: 'evm_mine', id: Date.now(), });
web3.currentProvider.send({ jsonrpc: '2.0', method: 'evm_mine', id: Date.now(), });
web3.currentProvider.send({ jsonrpc: '2.0', method: 'evm_mine', id: Date.now(), });
web3.currentProvider.send({ jsonrpc: '2.0', method: 'evm_mine', id: Date.now(), });
web3.currentProvider.send({ jsonrpc: '2.0', method: 'evm_mine', id: Date.now(), });
web3.currentProvider.send({ jsonrpc: '2.0', method: 'evm_mine', id: Date.now(), });
web3.currentProvider.send({ jsonrpc: '2.0', method: 'evm_mine', id: Date.now(), });
web3.currentProvider.send({ jsonrpc: '2.0', method: 'evm_mine', id: Date.now(), });
web3.currentProvider.send({ jsonrpc: '2.0', method: 'evm_mine', id: Date.now(), });

race.startNewRace(1, { from: manager });

race.getCarPositions().then(xs => xs.map((x) => x.div('1e18').toString()))

// After finish

race.setWinnerCarIndex(0)

betting.distribute(0, { from: manager });

// GET YOUR MEONEY!!
///race.winnerCarIndex.call().then(console.log);

// console.log(`getCarPositions = ${await race.getCarPositions()}`);

// console.log(`winnerCarIndex = ${await race.winnerCarIndex.call()}`);

// await race.updateRaceState();

// console.log(`winnerCarIndex = ${await race.winnerCarIndex.call()}`);

// console.log(`getBalance(trackOwner) = ${await web3.eth.getBalance(trackOwner)}`);
// await race.claimTrackFee({from: trackOwner});
// console.log(`getBalance(trackOwner) = ${await web3.eth.getBalance(trackOwner)}`);
