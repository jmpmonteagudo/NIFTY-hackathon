pragma solidity 0.4.24;

import "../../node_modules/openzeppelin-solidity/contracts/token/ERC721/ERC721Token.sol";
import "./CryptoCarzControl.sol";

/// @title   CryptoCarzToken
/// @author  Jose Perez - <jose.perez@diginex.com>
/// @notice  
/// @dev     
contract CryptoCarzTokenTrack is ERC721Token, CryptoCarzControl {

    event CreateTrack(uint256 indexed tokenId);

    /// @dev Overrides ERC721Token's `canTransfer` modifier to check if contract is paused.
    /// @param _tokenId ID of the token to validate
    modifier canTransfer(uint256 _tokenId) {
        require(isApprovedOrOwner(msg.sender, _tokenId));
        require(!paused);
        _;
    }

    constructor(address _owner, address _manager)
        CryptoCarzControl(_owner, _manager)
        ERC721Token("CryptoCarzTokenTrack", "CARZT") public {
    }

    function createTrack(uint256 _tokenId) external ifNotPaused onlyManager {
        _mint(manager, _tokenId);
        emit CreateTrack(_tokenId);
    }
}
