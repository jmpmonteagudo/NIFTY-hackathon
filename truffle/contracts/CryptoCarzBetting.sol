pragma solidity 0.4.24;

import "../../node_modules/openzeppelin-solidity/contracts/math/SafeMath.sol";
import "./CryptoCarzControl.sol";
import "./CryptoCarzRace.sol";

contract CryptoCarzBetting is CryptoCarzControl {

    using SafeMath for uint256;

    CryptoCarzRace public race;
    
    mapping(address => uint256) public betAmounts;
    mapping(address => uint256) public betCarIds;
    uint256 public totalBetAmount;
    address[] public betters;
    uint256 public ethToClaim;
    uint256 public numWinners;
    mapping(address => bool) public isWinner;
    mapping(address => bool) public claimed;
    bool public distributed;

    event Bet(address indexed better, uint256 carId, uint256 betAmount, uint256 accumulatedBetAmount);

    constructor(
        address _owner, address _manager, address _race)
        CryptoCarzControl(_owner, _manager) public {

        require(_race != address(0));
        race = CryptoCarzRace(_race);
    }

    function() external payable {
    }

    function isWinner(address better) external view returns(bool) {
        return isWinner[better];
    }

    function bet(uint256 _carId) external payable {
        // TODO: only allowed if race has not yet started?
        require(msg.value > 0);

        uint256 currentAmount = betAmounts[msg.sender];
        betAmounts[msg.sender] = currentAmount.add(msg.value);
        if (currentAmount == 0) {
            betters.push(msg.sender);
        }
        totalBetAmount = totalBetAmount.add(msg.value);
        betCarIds[msg.sender] = _carId;

        emit Bet(msg.sender, _carId, msg.value, betAmounts[msg.sender]);
    }


    function distribute(uint who) external {
        // require(race.getWinner() != address(0));
        // require(!distributed);

        // for (uint i = 0; i < betters.length; i++) {
        //     if (betCarIds[betters[i]] == race.getWinnerCarIndex()) {
        //         numWinners++;
        //     }
        // }

        isWinner[betters[0]] = true;
        ethToClaim = 0.03 ether;
        distributed = true;
    }

    function claim() external {
        require(isWinner[msg.sender]);
        require(!claimed[msg.sender]);

        claimed[msg.sender] = true;
        msg.sender.transfer(ethToClaim);
    }
}
