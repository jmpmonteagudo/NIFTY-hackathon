pragma solidity 0.4.24;

import "./CryptoCarzControl.sol";

/// @title   CryptoCarzOracle
/// @author  Jose Perez - <jose.perez@diginex.com>
/// @notice  Tamper-proof record of exchange rates e.g. BTC/USD, ETC/USD, etc.
/// @dev     Exchange rates are updated from off-chain server periodically. Rates are taken from a
///          publicly available third-party provider, such as Coinbase, CoinMarketCap, etc.
contract CryptoCarzOracle is CryptoCarzControl {

    mapping(string => uint256) internal currentRates;

    event RateUpdated(string id, uint256 rate);

    /// @dev The ExchangeRate constructor.
    /// @param _owner The contract owner.
    /// @param _manager The contract _manager.
    constructor(address _owner, address _manager)
        CryptoCarzControl(_owner, _manager) public {

            // Set initial rate 
            currentRates["BTC"] = 8000; 
            currentRates["ETH"] = 450; 
            currentRates["XRP"] = 800; 
            currentRates["BTH"] = 1; 
    }

    /// @dev Allows the current updater account to update a single rate.
    /// @param _id The rate identifier.
    /// @param _rate The exchange rate.
    function updateRate(string _id, uint256 _rate) external onlyManager {
        require(_rate != 0);
        currentRates[_id] = _rate;
        emit RateUpdated(_id, _rate);
    }

    /// @dev Allows anyone to read the current rate.
    /// @param _id The rate identifier.
    /// @return The current rate.
    function getRate(string _id) external view returns(uint256) {
        return currentRates[_id];
    }
}
