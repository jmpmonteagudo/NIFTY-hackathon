pragma solidity 0.4.24;

import "../../node_modules/openzeppelin-solidity/contracts/math/SafeMath.sol";
import "../../node_modules/openzeppelin-solidity/contracts/token/ERC721/ERC721Receiver.sol";
import "./CryptoCarzToken.sol";
import "./CryptoCarzControl.sol";
import "./CryptoCarzOracle.sol";

/// @title   CryptoCarzRace
contract CryptoCarzRace is CryptoCarzControl {

    using SafeMath for uint256;

    CryptoCarzToken public tokenCars;
    CryptoCarzToken public tokenTracks;
    CryptoCarzOracle public oracle;

    uint256 public startingBlockNum;
    uint256 public endBlockNum;
    uint256[] public carIds;
    address[] public carOwners;
    address public trackOwner;
    uint256 public raceBalance;
    uint256 public winnerCarIndex;
    address public winner;

    uint256 public constant FINISH_LINE = 100;

    mapping(uint256 => uint256) public initialRates;

    constructor(
        address _owner, address _manager, address _tokenCars, address _tokenTracks, address _oracle)
        CryptoCarzControl(_owner, _manager) public {

        require(_tokenCars != address(0));
        require(_tokenTracks != address(0));
        require(_oracle != address(0));
        tokenCars = CryptoCarzToken(_tokenCars);
        tokenTracks = CryptoCarzToken(_tokenTracks);
        oracle = CryptoCarzOracle(_oracle);
    }

    function mapRate(uint256 i) public returns (string) {
        if (i == 0) { return "BTC"; }
        if (i == 1) { return "ETH"; }
        if (i == 2) { return "XRP"; }
        if (i == 3) { return "BCH"; }
        return "";
    }

    function getWinner() external view returns(address) {
        return winner;
    }

    function getWinnerCarIndex() external view returns(uint256) {
        return winnerCarIndex;
    }

    function setWinnerCarIndex(uint i) external {
      winnerCarIndex = i;
      winner = carOwners[i];
    }

    function() external payable {
        revert();
    }    

    function signUp(uint256 _carId) payable external {
        require(tokenCars.ownerOf(_carId) == msg.sender);
        require(msg.value == 1 ether);
        carIds.push(_carId);
        carOwners.push(msg.sender);
        raceBalance = raceBalance.add(msg.value);
    }

    function startNewRace(uint256 _trackId) external onlyManager {
        startingBlockNum = block.number;

        for (uint256 i = 0; i < carIds.length; i++) {
            initialRates[i] = oracle.getRate(mapRate(i));
        }

        trackOwner = tokenTracks.ownerOf(_trackId);

        // check that all participants sent their cars to the race contract
        for (i = 0; i < carIds.length; i++) {
            require(tokenCars.ownerOf(carIds[i]) == address(this));
        }
    }

    function getCarPosition(uint256 _i) external view returns(uint256) {
        uint256 currentRate = oracle.getRate(mapRate(_i));
        uint256 position = currentRate.sub(initialRates[_i]);
        return position;
    }

    function updateRaceState() external {
        uint256 fastestCarIndex;
        uint256 slowestCarIndex;
        for (uint256 i = 0; i < carIds.length; i++) {
            uint256 pos = this.getCarPosition(i);
            if (pos > fastestCarIndex) {
                fastestCarIndex = i;
            }
            if (slowestCarIndex == 0 || pos < slowestCarIndex) {
                slowestCarIndex = i;
            }
        }

        if (this.getCarPosition(fastestCarIndex) >= FINISH_LINE) {
            winnerCarIndex = fastestCarIndex;
            winner = carOwners[winnerCarIndex];
            // TODO: update car XP / lvl
        }
    }

    function getCarPositions() public view returns(uint256[]) {
        uint256[] memory pos = new uint256[](carIds.length);
        for (uint256 i = 0; i < carIds.length; i++) {
            pos[i] = this.getCarPosition(i);
        }
        return pos;
    }

    function claimTrophy() external {
        require(winner != address(0));
        require(msg.sender == carOwners[winnerCarIndex]);
        
        // send all cars to the winner
        for (uint256 i = 0; i < carIds.length; i++) {
            tokenCars.safeTransferFrom(address(this), msg.sender, carIds[i]);
        }
    }

    function claimTrackFee() external {
        require(msg.sender == trackOwner);
        uint256 rateBalanceToSend = raceBalance;
        raceBalance = 0;
        msg.sender.transfer(rateBalanceToSend);
    }
}
