"use strict";

const CryptoCarzRace = artifacts.require('./CryptoCarzRace.sol');
const CryptoCarzTokenTrack = artifacts.require('./CryptoCarzTokenTrack.sol');
const CryptoCarzToken = artifacts.require('./CryptoCarzToken.sol');
const CryptoCarzOracle = artifacts.require('./CryptoCarzOracle.sol');
const CryptoCarzBetting = artifacts.require('./CryptoCarzBetting.sol');

const fs = require('fs');
import assertRevert from './assertRevert';
import increaseBlocks from './increaseBlocks';
import constants from './constants';
const BigNumber = web3.BigNumber;
require('chai')
    .use(require('chai-as-promised'))
    .use(require('chai-bignumber')(BigNumber))
    .should();

contract('CryptoCarzRace', function (accounts) {

    const ETHER = new BigNumber('1e18');
    const DEC18 = new BigNumber('1e18');


    const CAR_IDS = [1, 2, 3, 4];

    const NUM_ACCOUNTS = process.env.NUM_ACCOUNTS;
    const owner = accounts[NUM_ACCOUNTS - 1];
    const manager = accounts[NUM_ACCOUNTS - 2];
    const treasurer = accounts[NUM_ACCOUNTS - 3];
    const someoneElse = accounts[NUM_ACCOUNTS - 4];
    const trackOwner = accounts[NUM_ACCOUNTS - 5];
    const users = accounts.slice(1, NUM_ACCOUNTS - 6);
    const participants = users.slice(0, 4);
    const betters = users.slice(4, 8);
    console.log(`participants = ${JSON.stringify(participants)}`);
    console.log(`betters = ${JSON.stringify(betters)}`);

    let tokenCars;
    let tokenTracks;
    let race;
    let oracle;
    let betting;

    describe('whole race', async function () {
        it('initialization', async function () {

            tokenCars = await CryptoCarzToken.new(owner, manager, treasurer, { from: someoneElse });
            console.log(`tokenCars.address = ${tokenCars.address}`);

            tokenTracks = await CryptoCarzTokenTrack.new(owner, manager, { from: someoneElse });
            console.log(`tokenTracks.address = ${tokenTracks.address}`);

            oracle = await CryptoCarzOracle.new(owner, manager, { from: someoneElse });
            console.log(`oracle.address = ${oracle.address}`);

            race = await CryptoCarzRace.new(
                owner, manager, tokenCars.address, tokenTracks.address, oracle.address, { from: someoneElse });
            console.log(`race.address = ${race.address}`);

            betting = await CryptoCarzBetting.new(owner, manager, race.address);
            console.log(`betting.address = ${betting.address}`);

            // save contracts addresses
            const data = { "CryptoCarzBetting": betting.address };
            fs.writeFileSync('../build/contractAddresses.txt', JSON.stringify(data, null, 4));

            await tokenCars.createSeries(CAR_IDS.length, { from: manager });
            await tokenCars.createCars(CAR_IDS, 0, { from: manager });
            await tokenTracks.createTrack(1, { from: manager });

            await tokenCars.transferFrom(manager, participants[0], CAR_IDS[0], { from: manager });
            await tokenCars.transferFrom(manager, participants[1], CAR_IDS[1], { from: manager });
            await tokenCars.transferFrom(manager, participants[2], CAR_IDS[2], { from: manager });
            await tokenCars.transferFrom(manager, participants[3], CAR_IDS[3], { from: manager });

            await tokenTracks.transferFrom(manager, trackOwner, 1, { from: manager });


            await oracle.updateRate("BTC", DEC18.times(6000), { from: manager });
            await oracle.updateRate("ETH", DEC18.times(400), { from: manager });
            await oracle.updateRate("XRP", DEC18.times(1.20), { from: manager });
            await oracle.updateRate("BCH", DEC18.times(800), { from: manager });

            console.log(`${await oracle.getRate("BTC")}`);

        });

        it('participants sign up to the race', async function () {
            for (let i = 0; i < participants.length; i++) {
                await race.signUp(CAR_IDS[i], { from: participants[i], value: ETHER.times(1) });
            }
        });

        it('participants send their cars to the race contract', async function () {
            for (let i = 0; i < participants.length; i++) {
                console.log(`ownerof(${CAR_IDS[i]}) = ${await tokenCars.ownerOf(CAR_IDS[i])}`);
                await tokenCars.transferFrom(participants[i], race.address, CAR_IDS[i], { from: participants[i] });
                console.log(`ownerof(${CAR_IDS[i]}) = ${await tokenCars.ownerOf(CAR_IDS[i])}`);
            }
            console.log(`race.address = ${race.address}`);

        });

        it('betters place their bets', async function () {
            await betting.bet(0, { from: betters[0], value: ETHER.times(1) });
            await betting.bet(1, { from: betters[1], value: ETHER.times(1) });
            await betting.bet(2, { from: betters[2], value: ETHER.times(1) });
            await betting.bet(3, { from: betters[3], value: ETHER.times(1) });
            console.log(`betting.totalBetAmount = ${await betting.totalBetAmount.call()}`);
        });

        it('race starts', async function () {
            await race.startNewRace(1, { from: manager });

            console.log(`${await race.getCarPosition(1)}`);

            await increaseBlocks(1);

            await oracle.updateRate("BTC", DEC18.times(6600), { from: manager });
            await oracle.updateRate("ETH", DEC18.times(440), { from: manager });
            await oracle.updateRate("XRP", DEC18.times(1.32), { from: manager });
            await oracle.updateRate("BCH", DEC18.times(880), { from: manager });


            console.log(`${await race.getCarPosition(0)}`);
            console.log(`${await race.getCarPosition(1)}`);
            console.log(`${await race.getCarPosition(2)}`);
            console.log(`${await race.getCarPosition(3)}`);

            console.log(`getCarPositions = ${await race.getCarPositions()}`);
        });

        it('race ends', async function () {
            console.log(`winnerCarIndex = ${await race.winnerCarIndex.call()}`);

            await race.updateRaceState();

            console.log(`winnerCarIndex = ${await race.winnerCarIndex.call()}`);

        });

        it('track owner claims fee', async function () {
            console.log(`getBalance(trackOwner) = ${await web3.eth.getBalance(trackOwner)}`);
            console.log(`race.raceBalance = ${await race.raceBalance.call()}`);
            await race.claimTrackFee({ from: trackOwner });
            console.log(`getBalance(trackOwner) = ${await web3.eth.getBalance(trackOwner)}`);
            console.log(`race.raceBalance = ${await race.raceBalance.call()}`);
        });

        it('winner claims trophy', async function () {
            const winner = await race.getWinner();
            console.log(`winner = ${winner}`);
            console.log(`tokenCars.balanceOf(winner) = ${await tokenCars.balanceOf(winner)}`);
            console.log(`race.winner = ${await race.winner.call()}`);
            console.log(`race.winnerCarIndex = ${await race.winnerCarIndex.call()}`);

            await race.claimTrophy({ from: winner });
            console.log(`tokenCars.balanceOf(winner) = ${await tokenCars.balanceOf(winner)}`);
        });

        it('betting winners get their prize', async function () {
            await betting.distribute({ from: manager });

            
            console.log(`betting.totalBetAmount = ${await betting.totalBetAmount.call()}`);
            console.log(`betting.ethToClaim = ${await betting.ethToClaim.call()}`);

            for (let i = 0; i < betters.length; i++) {
                if (await betting.isWinner(betters[i])) {
                    console.log(`Betting winner: ${betters[i]} - balance = ${await web3.eth.getBalance(betters[i])}`);
                }
            }

            for (let i = 0; i < betters.length; i++) {
                if (await betting.isWinner(betters[i])) {
                    await betting.claim({ from: betters[i] });
                    console.log(`Betting winner: ${betters[i]} - balance = ${await web3.eth.getBalance(betters[i])}`);
                }
            }

        });
    });
});
