Web3 = require("web3")

BigNumber = require('bignumber.js')
DEC18 = new BigNumber('1e18');

function gaussianRand() {
  var rand = 0;

  for (var i = 0; i < 6; i += 1) {
    rand += Math.random();
  }

  return rand / 10;
}

web3 = new Web3(new Web3.providers.HttpProvider("http://localhost:9545"))
abi = require("./truffle/build/contracts/CryptoCarzOracle").abi

manager = "0xf17f52151ebef6c7334fad080c5704d77216b732"
oracleAddress = "0x57dae64f612f50d10381476faafd625fb3552652"

oracle = web3.eth.contract(abi).at(oracleAddress)

z0 = 1
z1 = 1
z2 = 1
z3 = 1

function go() {
  z0 += gaussianRand() * 0.3
  z1 += gaussianRand() * 0.3
  z2 += gaussianRand() * 0.3
  z3 += gaussianRand() * 0.3

  z0 = Math.min(z0, 2)
  z1 = Math.min(z1, 2)
  z2 = Math.min(z2, 2)
  z3 = Math.min(z3, 2)

  if (z0 == 2) {
    z1 = Math.min(z1, 1.99)
    z2 = Math.min(z2, 1.99)
    z3 = Math.min(z3, 1.99)
  }

  console.log("Set 0 = " + z0);
  console.log("Set 1 = " + z1);
  console.log("Set 2 = " + z2);
  console.log("Set 3 = " + z3);

  oracle.updateRate("BTC", DEC18.times(z0.toString()).toFixed(), { from: manager });
  oracle.updateRate("ETH", DEC18.times(z1.toString()).toFixed(), { from: manager });
  oracle.updateRate("XRP", DEC18.times(z2.toString()).toFixed(), { from: manager });
  oracle.updateRate("BCH", DEC18.times(z3.toString()).toFixed(), { from: manager });

  if (z0 >= 2 || z1 >= 2 || z2 >= 2 || z3 >= 2) {
  } else {
    setTimeout(go, 1000);
  }
}

go();
