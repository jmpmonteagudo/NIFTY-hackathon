import * as DCL from 'decentraland-api';
import * as assert from 'assert';

import { createElement, BillboardModes } from 'decentraland-api';

const TRACK_LENGTH = 100
const DISTANCE_FORMULA_CONSTANT = 2;

class DistanceFormula {
  private readonly vs: number[] = [0];
  private readonly as: number[] = [2];
  private readonly ps: number[] = [];

  constructor(private readonly c: number) { }

  calculate(t: number, p: number): number {
    assert(t > 0);

    if (t == 1) { this.ps.push(p); }

    this.ps[t] = p;
    const priceChangePct = (this.ps[t] - this.ps[t - 1]) / this.ps[t - 1];
    const a_c = this.as[t - 1] * 0.95;

    this.as[t] = a_c * (1 + priceChangePct * this.c);
    const v = this.vs[t] = this.vs[t - 1] + this.as[t - 1];
    const s = t * v;

    return s;
  }
}

interface State {
  camera: DCL.Vector3Component

  car1: DCL.Vector3Component
  car2: DCL.Vector3Component
  car3: DCL.Vector3Component

  cameraTransition: { position: { duration: number } }
  message: string
}

// const RACE_CONTRACT_ABI = require('../truffle/build/contracts/CryptoCarzRace.json').abi;
// const RACE_CONTRACT_ADDRESS = '0x37845e77645d393ac9a375dfc58c7bf885478a8f'
// const JSON_RPC_URL = 'http://localhost:9545'

export default class SampleScene extends DCL.ScriptableScene {
  state: State = {
    // camera: { x: 5, y: 0, z: 2.85 },
    camera: { x: 5, y: 0, z: 5 },
    car1: { x: 10, y: 0, z: 5 },
    car2: { x: 15, y: 0, z: 5 },
    car3: { x: 20, y: 0, z: 5 },

    cameraTransition: { position: { duration: 0 } },
    message: ""
  }

  private readonly car0formula = new DistanceFormula(DISTANCE_FORMULA_CONSTANT);
  private readonly car1formula = new DistanceFormula(DISTANCE_FORMULA_CONSTANT);
  private readonly car2formula = new DistanceFormula(DISTANCE_FORMULA_CONSTANT);
  private readonly car3formula = new DistanceFormula(DISTANCE_FORMULA_CONSTANT);

  raceStarted = false
  lastUpdate = 0;
  
  updateDistances(distances: number[]){
    let message = "";

    if (distances.some((d) => d >= 1.0)) {
      if (this.state.camera.z >= TRACK_LENGTH) {
        message = "You win!"
      } else if (
        this.state.car1.z >= TRACK_LENGTH ||
        this.state.car2.z >= TRACK_LENGTH ||
        this.state.car3.z >= TRACK_LENGTH
      ) {
        message = "You lose!"
      }
    }

    const now = performance.now();
    if (!this.lastUpdate) {
      this.lastUpdate = now;
      return;
    }

    this.setState({
      ...this.state,

      camera: { ...this.state.camera, z: distances[0] * (TRACK_LENGTH - 5) + 5 },
      car1: { ...this.state.car1, z: distances[1] * (TRACK_LENGTH - 5) + 5 },
      car2: { ...this.state.car2, z: distances[2] * (TRACK_LENGTH - 5) + 5 },
      car3: { ...this.state.car3, z: distances[3] * (TRACK_LENGTH - 5) + 5 },

      cameraTransition: { position: { duration: now - this.lastUpdate } },
      message
    })

    this.lastUpdate = now;
  }

  readonly camera0 = { ...this.state.camera };

  sceneDidMount() {
    setTimeout(() => {
      this.raceStarted = true;

      const ws = new WebSocket('wss://stream.binance.com:9443/stream?streams=btcusdt@ticker/ethusdt@ticker/xrpusdt@ticker/eosusdt@ticker');

      let car0s = 0;
      let car1s = 0;
      let car2s = 0;
      let car3s = 0;

      let carTime0 = 0;
      let carTime1 = 0;
      let carTime2 = 0;
      let carTime3 = 0;

      let counter0 = 0;
      let counter1 = 0;
      let counter2 = 0;
      let counter3 = 0;

      let acc: number | null = null;
      
      ws.onmessage = (dat) => {
        const j = JSON.parse(dat.data)['data'];

        const someoneWon = car0s >= 1 || car1s >= 1 || car2s >= 1 || car3s >= 1;

        switch (j['s']) {
          case 'BTCUSDT':
            if (j['E'] > carTime0) {
              carTime0 = j['E'];
              if (counter0 == counter1 && counter1 == counter2 && counter2 == counter3) {
                counter0 += 1;
              } else if (counter0 < Math.max(counter1, counter2, counter3)) {
                counter0 += 1;
              }

              if (!someoneWon) {
                car0s = this.car0formula.calculate(counter0, j['w']) / 1000;
              }
            }
            break;

          case 'ETHUSDT':
            if (j['E'] > carTime1) {
              carTime1 = j['E'];
              if (counter0 == counter1 && counter1 == counter2 && counter2 == counter3) {
                counter1 += 1;
              } else if (counter1 < Math.max(counter0, counter2, counter3)) {
                counter1 += 1;
              }

              if (!someoneWon) {
                car1s = this.car1formula.calculate(counter1, j['w']) / 500;
              }
            }
            break;

          case 'XRPUSDT':
            if (j['E'] > carTime2) {
              carTime2 = j['E'];
              if (counter0 == counter1 && counter1 == counter2 && counter2 == counter3) {
                counter2 += 1;
              } else if (counter2 < Math.max(counter0, counter1, counter3)) {
                counter2 += 1;
              }

              if (!someoneWon) {
                car2s = this.car2formula.calculate(counter2, j['w']) / 500;
              }
            }
            break;

          case 'EOSUSDT':
            if (j['E'] > carTime3) {
              carTime3 = j['E'];
              if (counter0 == counter1 && counter1 == counter2 && counter2 == counter3) {
                counter3 += 1;
              } else if (counter3 < Math.max(counter0, counter1, counter2)) {
                counter3 += 1;
              }

              if (!someoneWon) {
                car3s = this.car3formula.calculate(counter3, j['w']) / 500;
              }
            }
            break;
        }

        car0s = Math.min(1.0, car0s);
        car1s = Math.min(1.0, car1s);
        car2s = Math.min(1.0, car2s);
        car3s = Math.min(1.0, car3s);

        if (counter0 !== 0 && Math.min(counter0, counter1, counter2, counter3) === Math.max(counter0, counter1, counter2, counter3)) {
          this.updateDistances([car0s, car1s, car2s, car3s]);
        }
      };
    }, 0);
  }

  // sceneWillUnmount() {
  //   clearInterval(this.timeout)
  // }

  positionFor(coords: DCL.Vector3Component): DCL.Vector3Component {
    return {
      x: coords.x - this.state.camera.x + this.camera0.x,
      y: coords.y - this.state.camera.y + this.camera0.y,
      z: coords.z - this.state.camera.z + this.camera0.z,
    }
  }

  async render() {

    return (
      <scene position={{ x: 0, y: 0, z: -8.5 }}>
        <material id="crypto-icon-1" albedoTexture="./materials/crypto-icon.png" />
        <material id="crypto-icon-2" albedoTexture="./materials/crypto-icon-2.png" />
        <material id="crypto-icon-3" albedoTexture="./materials/crypto-icon-3.png" />
        <material id="crypto-icon-4" albedoTexture="./materials/crypto-icon-4.png" />

        <entity 
          transition={this.state.cameraTransition}
          position={this.camera0}
        >
          <gltf-model scale={0.5} rotation={{ x: 0, y: 180, z: 0 }} src="./models/MONERO.glb" />
          <plane
            material="#crypto-icon-1"
            scale={{ x: 0.4, y: 0.4, z: 0.4 }}
            billboard={BillboardModes.BILLBOARDMODE_ALL as any}
            position={{ x: 0, y: 2.6, z: 0 }} />
          <text
            value="Owner: 0x8A42343C30"
            width={10} height={10}
            billboard={BillboardModes.BILLBOARDMODE_ALL as any}
            position={{ x: 0, y: 2.1, z: 0}} />
          <text
            value="Win rate: 10/14"
            width={10} height={10}
            billboard={BillboardModes.BILLBOARDMODE_ALL as any}
            position={{ x: 0, y: 1.7, z: 0}} />
        </entity>

        <entity
          transition={this.state.cameraTransition}
          position={this.positionFor(this.state.car1)}
        >
          <gltf-model scale={0.5} rotation={{ x: 0, y: 180, z: 0 }} src="./models/ZEROx.glb" />
          <plane
            material="#crypto-icon-2"
            scale={{ x: 0.4, y: 0.4, z: 0.4 }}
            billboard={BillboardModes.BILLBOARDMODE_ALL as any}
            position={{ x: 0, y: 2.6, z: 0 }} />
          <text
            value="0x2EEA7c7716C"
            width={10} height={10}
            billboard={BillboardModes.BILLBOARDMODE_ALL as any}
            position={{ x: 0, y: 2.1, z: 0}} />
          <text
            value="Win rate: 100/1337"
            width={10} height={10}
            billboard={BillboardModes.BILLBOARDMODE_ALL as any}
            position={{ x: 0, y: 1.7, z: 0}} />
        </entity>

        <entity
          transition={this.state.cameraTransition}
          position={this.positionFor(this.state.car2)}
        >
          <gltf-model scale={0.5} rotation={{ x: 0, y: 180, z: 0 }} src="./models/BTCC.glb" />
          <plane
            material="#crypto-icon-3"
            scale={{ x: 0.4, y: 0.4, z: 0.4 }}
            billboard={BillboardModes.BILLBOARDMODE_ALL as any}
            position={{ x: 0, y: 2.6, z: 0 }} />
          <text
            value="0x468cB738f24"
            width={10} height={10}
            billboard={BillboardModes.BILLBOARDMODE_ALL as any}
            position={{ x: 0, y: 2.1, z: 0}} />
          <text
            value="Win rate: 3/8"
            width={10} height={10}
            billboard={BillboardModes.BILLBOARDMODE_ALL as any}
            position={{ x: 0, y: 1.7, z: 0}} />
        </entity>

        <entity
          transition={this.state.cameraTransition}
          position={this.positionFor(this.state.car3)}
        >
          <gltf-model scale={0.5} rotation={{ x: 0, y: 180, z: 0 }} src="./models/BTC.glb" />
          <plane
            material="#crypto-icon-4"
            scale={{ x: 0.4, y: 0.4, z: 0.4 }}
            billboard={BillboardModes.BILLBOARDMODE_ALL as any}
            position={{ x: 0, y: 2.6, z: 0 }} />
          <text
            value="0x08D87f30F11"
            width={10} height={10}
            billboard={BillboardModes.BILLBOARDMODE_ALL as any}
            position={{ x: 0, y: 2.1, z: 0}} />
          <text
            value="Win rate: 14/20"
            width={10} height={10}
            billboard={BillboardModes.BILLBOARDMODE_ALL as any}
            position={{ x: 0, y: 1.7, z: 0}} />
        </entity>


        <gltf-model
          scale={1}
          transition={this.state.cameraTransition}
          rotation={{ x: 0, y: 180, z: 0 }}
          position={this.positionFor({ x: -15, y: 0.0, z: -175 })}
          src='./models/ENV.glb'
        />

        <text
          transition={this.state.cameraTransition}
          position={this.positionFor({ x: 15, y: 8, z: 105 })}
          fontSize={1500}
          width={15} height={10}
          value={this.state.message} />
      </scene>
    )
  }
}
